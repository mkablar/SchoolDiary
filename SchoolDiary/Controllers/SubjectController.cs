﻿using NLog;
using SchoolDiary.Models;
using SchoolDiary.Models.DTOs;
using SchoolDiary.Models.DTOs.UpdateDTOs;
using SchoolDiary.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace SchoolDiary.Controllers
{
    [RoutePrefix("api/subjects")]
    public class SubjectController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private ISubjectService subjectService;
        private ITeacherService teacherService;
        private IStudentService studentService;
        private ITeacherSubjectService tsService;
        private IStudentSubjectMarksService ssmService;

        public SubjectController(ISubjectService subjectService, ITeacherService teacherService, IStudentService studentService, ITeacherSubjectService tsService, IStudentSubjectMarksService ssmService)
        {
            this.subjectService = subjectService;
            this.teacherService = teacherService;
            this.studentService = studentService;
            this.tsService = tsService;
            this.ssmService = ssmService;
        }

        // GET: api/subjects
        // [Authorize]
        [Route("")]
        [ResponseType(typeof(PublicSubjectDTO))]
        public IEnumerable<PublicSubjectDTO> Get()
        {
            logger.Info("Requesting subjects");

            return subjectService.Get().Select(x => new PublicSubjectDTO(x));
        }

        // GET: api/subjects/5
        // [Authorize]
        [Route("{id:int}")]
        [ResponseType(typeof(PublicSubjectDTO))]
        public IHttpActionResult GetByIdPublic(int id)
        {
            logger.Info("Requesting a subject");

            Subject subject = subjectService.GetById(id);
            if (subject == null)
            {
                return NotFound();
            }

            return Ok(new PublicSubjectDTO(subject));
        }

        // POST: api/subjects
        // [Authorize(Roles = "admins")]
        [Route("")]
        [ResponseType(typeof(PublicSubjectDTO))]
        public IHttpActionResult PostSubject(SubjectDTO subjectModel)
        {
            logger.Info("Requesting to insert a subject");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (subjectService.Get().Any(x => x.Name == subjectModel.Name))
            {
                return BadRequest("That name is already in use.");
            }

            return Ok(new PublicSubjectDTO(subjectService.Insert(subjectModel)));
        }

        // PUT: api/subjects
        // [Authorize(Roles = "admins")]
        [Route("{id}")]
        [ResponseType(typeof(PublicSubjectDTO))]
        public IHttpActionResult PutSubject(int id, UpdateSubjectDTO subjectModel)
        {
            logger.Info("Requesting to change a subject");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Subject subject = subjectService.GetById(id);
            if (subject == null || subject.Id != subjectModel.Id)
            {
                return NotFound();
            }

            if (subject.Name != subjectModel.Name && subjectService.Get().Any(x => x.Name == subjectModel.Name))
            {
                return BadRequest("That name is already in use.");
            }

            return Ok(new PublicSubjectDTO(subjectService.Update(subject, subjectModel)));
        }

        // PUT: api/subjects/5/addteacher/5
        // [Authorize(Roles = "admins")]
        [Route("{subjectId:int}/addteacher/{teacherId}")]
        [ResponseType(typeof(PublicSubjectDTO))]
        public IHttpActionResult PutSubjectAddTeacher(int subjectId, string teacherId)
        {
            logger.Info("Requesting to add teacher to a subject");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Subject subject = subjectService.GetById(subjectId);
            Teacher teacher = teacherService.GetById(teacherId);

            if (subject == null || teacher == null)
            {
                return NotFound();
            }

            if (tsService.GetByTeacherAndSubject(teacher, subject) != null)
            {
                return BadRequest("That teacher already teaches that subject.");
            }

            return Ok(new PublicSubjectDTO(subjectService.AddTeacher(teacher, subject)));
        }

        // PUT: api/subjects/5/removeteacher/5
        // [Authorize(Roles = "admins")]
        [Route("{subjectId:int}/removeteacher/{teacherId}")]
        [ResponseType(typeof(PublicSubjectDTO))]
        public IHttpActionResult PutSubjectRemoveTeacher(int subjectId, string teacherId)
        {
            logger.Info("Requesting to remove a teacher from a subject");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Subject subject = subjectService.GetById(subjectId);
            Teacher teacher = teacherService.GetById(teacherId);

            if (subject == null || teacher == null)
            {
                return NotFound();
            }

            TeacherSubject teacherSubject = tsService.GetByTeacherAndSubject(teacher, subject);
            if (teacherSubject == null)
            {
                return BadRequest("That teacher does not teach that subject.");
            }

            return Ok(new PublicSubjectDTO(subjectService.RemoveTeacher(teacherSubject)));
        }

        // PUT: api/subjects/5/addstudent/5
        // [Authorize(Roles = "admins")]
        [Route("{subjectId:int}/addstudent/{studentId}")]
        [ResponseType(typeof(PublicSubjectDTO))]
        public IHttpActionResult PutSubjectAddStudent(int subjectId, string studentId)
        {
            logger.Info("Requesting to add student to a subject");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Subject subject = subjectService.GetById(subjectId);
            Student student = studentService.GetById(studentId);

            if (subject == null || student == null)
            {
                return NotFound();
            }

            if (ssmService.GetByStudentAndSubject(student, subject) != null)
            {
                return BadRequest("That student already attends that subject.");
            }

            return Ok(new PublicSubjectDTO(subjectService.AddStudent(student, subject)));
        }

        // PUT: api/subjects/5/removestudent/5
        // [Authorize(Roles = "admins")]
        [Route("{subjectId:int}/removestudent/{studentId}")]
        [ResponseType(typeof(PublicSubjectDTO))]
        public IHttpActionResult PutSubjectRemoveStudent(int subjectId, string studentId)
        {
            logger.Info("Requesting to remove student from a subject");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Subject subject = subjectService.GetById(subjectId);
            Student student = studentService.GetById(studentId);

            if (subject == null || student == null)
            {
                return NotFound();
            }

            StudentSubjectMarks studentSubject = ssmService.GetByStudentAndSubject(student, subject);
            if (studentSubject == null)
            {
                return BadRequest("That student does not attend that subject.");
            }

            return Ok(new PublicSubjectDTO(subjectService.RemoveStudent(studentSubject)));
        }

        // DELETE: api/subjects/5
        // [Authorize(Roles = "admins")]
        [Route("{id:int}")]
        [ResponseType(typeof(PublicSubjectDTO))]
        public IHttpActionResult DeleteSubject(int id)
        {
            logger.Info("Requesting to delete a subject");

            Subject subject = subjectService.GetById(id);
            if (subject == null)
            {
                return NotFound();
            }

            return Ok(new PublicSubjectDTO(subjectService.Delete(subject)));
        }
    }
}