﻿using NLog;
using SchoolDiary.Models;
using SchoolDiary.Models.DTOs;
using SchoolDiary.Models.DTOs.UpdateDTOs;
using SchoolDiary.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace SchoolDiary.Controllers
{
    [RoutePrefix("api/marks")]
    public class MarkController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private IMarkService markService;
        private IStudentService studentService;
        private IUserService userService;

        public MarkController(IMarkService markService, IUserService userService, IStudentService studentService)
        {
            this.markService = markService;
            this.userService = userService;
            this.studentService = studentService;
        }

        // GET: api/marks/5
        // [Authorize(Roles = "admins, teachers, parents")]
        [Route("{id:int}")]
        [ResponseType(typeof(AdminMarkDTO))]
        public IHttpActionResult GetById(int id)
        {
            logger.Info("Requesting a mark");

            // string userId = ((ClaimsPrincipal)RequestContext.Principal).FindFirst(x => x.Type == "UserId").Value;

            Mark mark = markService.GetById(id);

            if (mark == null)
            {
                return NotFound();
            }

            // if (!RequestContext.Principal.IsInRole("admins") && !studentService.CheckIfParent(userId, mark.StudentSubjectMarks.Student) && !studentService.CheckIfTeacher(userId, mark.StudentSubjectMarks.Student))
            // {
            //     return BadRequest("You do not have the authority to view this content");
            // }

            return Ok(new AdminMarkDTO(mark));
        }

        // PUT: api/marks/5
        // [Authorize(Roles = "admins, teachers")]
        [Route("{id:int}")]
        [ResponseType(typeof(AdminMarkDTO))]
        public IHttpActionResult PutMark(int id, UpdateMarkDTO markModel)
        {
            logger.Info("Requesting to change a mark");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mark mark = markService.GetById(id);
            if (mark == null || mark.Id != markModel.Id)
            {
                return NotFound();
            }

            ApplicationUser grader = userService.GetById(((ClaimsPrincipal)RequestContext.Principal).FindFirst(x => x.Type == "UserId").Value);
            if (!RequestContext.Principal.IsInRole("admins") && !(mark.StudentSubjectMarks.TeacherSubject.Teacher == grader))
            {
                return BadRequest("You do not have the authority to give a mark to this student for this subject.");
            }


            return Ok(new AdminMarkDTO(markService.Update(mark, grader, markModel)));
        }

        // DELETE: api/marks/5
        // [Authorize(Roles = "admins, teachers")]
        [Route("{id:int}")]
        [ResponseType(typeof(AdminMarkDTO))]
        public IHttpActionResult DeleteMark(int id)
        {
            logger.Info("Requesting to delete a mark");

            Mark mark = markService.GetById(id);
            if (mark == null)
            {
                return NotFound();
            }
            ApplicationUser grader = userService.GetById(((ClaimsPrincipal)RequestContext.Principal).FindFirst(x => x.Type == "UserId").Value);
            if (!RequestContext.Principal.IsInRole("admins") && !(mark.StudentSubjectMarks.TeacherSubject.Teacher == grader))
            {
                return BadRequest("You do not have the authority to delete this mark.");
            }

            return Ok(new AdminMarkDTO(markService.Delete(mark)));
        }
    }
}