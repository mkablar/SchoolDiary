﻿using NLog;
using SchoolDiary.Models;
using SchoolDiary.Models.DTOs.UpdateDTOs;
using SchoolDiary.Models.DTOs.UserDTOs;
using SchoolDiary.Models.DTOs.UserDTOs.TeacherDTOs;
using SchoolDiary.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace SchoolDiary.Controllers
{
    [RoutePrefix("api/teachers")]
    public class TeacherController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private ITeacherService teacherService;
        private IUserService userService;
        private ISubjectService subjectService;
        private ITeacherSubjectService tsService;
        private IStudentSubjectMarksService ssmService;

        public TeacherController(ITeacherService teacherService, IUserService userService, ISubjectService subjectService, ITeacherSubjectService tsService, IStudentSubjectMarksService ssmService)
        {
            this.teacherService = teacherService;
            this.userService = userService;
            this.subjectService = subjectService;
            this.tsService = tsService;
            this.ssmService = ssmService;
        }

        // GET: api/teachers
        // [Authorize]
        [Route("")]
        [ResponseType(typeof(AdminTeacherDTO))]
        public IEnumerable<PublicUserDTO> Get()
        {
            logger.Info("Requesting teachers");

            return teacherService.Get().Select(x => new AdminTeacherDTO(x));
        }

        // GET: api/teachers/5
        // [Authorize(Roles = "parents, students")]
        [Route("{id}")]
        [ResponseType(typeof(PublicTeacherDTO))]
        public IHttpActionResult GetByIdPublic(string id)
        {
            logger.Info("Student or parent requests teacher");

            Teacher teacher = teacherService.GetById(id);
            if (teacher == null)
            {
                return NotFound();
            }

            return Ok(new PublicTeacherDTO(teacher));
        }

        // GET: api/teachers/admin/5
        // [Authorize(Roles = "admins")]
        [Route("admin/{id}")]
        [ResponseType(typeof(AdminTeacherDTO))]
        public IHttpActionResult GetByIdAdmin(string id)
        {
            logger.Info("Admin requests teacher");

            Teacher teacher = teacherService.GetById(id);
            if (teacher == null)
            {
                return NotFound();
            }

            return Ok(new AdminTeacherDTO(teacher));
        }

        // GET: api/teachers/teacher/5
        // [Authorize(Roles = "teachers")]
        [Route("teacher/{id}")]
        [ResponseType(typeof(TeacherTeacherDTO))]
        public IHttpActionResult GetByIdTeacher(string id)
        {
            logger.Info("Teacher requests teacher");

            Teacher teacher = teacherService.GetById(id);
            if (teacher == null)
            {
                return NotFound();
            }

            return Ok(new TeacherTeacherDTO(teacher));
        }

        // PUT: api/teachers
        // [Authorize(Roles = "admins")]
        [Route("{id}")]
        [ResponseType(typeof(AdminTeacherDTO))]
        public IHttpActionResult PutTeacher(string id, UpdateTeacherDTO userModel)
        {
            logger.Info("Requesting to update teacher");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Teacher teacher = teacherService.GetById(id);
            if (teacher == null || teacher.Id != userModel.Id)
            {
                return NotFound();
            }

            // if either the email or username have changed, we need to check if the new email and/or username are available
            if ((teacher.UserName != userModel.UserName || teacher.Email != userModel.Email) && 
                userService.UserInDb(userModel.UserName, out string message, userModel.Email))
            {
                return BadRequest(message);
            }

            return Ok(new AdminTeacherDTO(teacherService.Update(teacher, userModel)));
        }

        // PUT: api/teachers/5/addsubject/5
        // [Authorize(Roles = "admins")]
        [Route("{teacherId}/addsubject/{subjectId:int}")]
        [ResponseType(typeof(AdminTeacherDTO))]
        public IHttpActionResult PutTeacherAddSubject(string teacherId, int subjectId)
        {
            logger.Info("Requesting to add subject to a teacher");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Teacher teacher = teacherService.GetById(teacherId);
            Subject subject = subjectService.GetById(subjectId);

            if (subject == null || teacher == null)
            {
                return NotFound();
            }

            if (tsService.GetByTeacherAndSubject(teacher, subject) != null)
            {
                return BadRequest("That teacher already teaches that subject.");
            }

            return Ok(new AdminTeacherDTO(teacherService.AddSubject(teacher, subject)));
        }

        // PUT: api/teachers/5/removesubject/5
        // [Authorize(Roles = "admins")]
        [Route("{teacherId}/removesubject/{subjectId:int}")]
        [ResponseType(typeof(AdminTeacherDTO))]
        public IHttpActionResult PutTeacherRemoveSubject(string teacherId, int subjectId)
        {
            logger.Info("Requesting to remove subject from a teacher");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Subject subject = subjectService.GetById(subjectId);
            Teacher teacher = teacherService.GetById(teacherId);

            if (subject == null || teacher == null)
            {
                return NotFound();
            }

            TeacherSubject teacherSubject = tsService.GetByTeacherAndSubject(teacher, subject);
            if (teacherSubject == null)
            {
                return BadRequest("That teacher does not teach that subject.");
            }

            return Ok(new AdminTeacherDTO(teacherService.RemoveSubject(teacherSubject)));
        }

        // PUT: api/teachers/5/addstudent/5
        // [Authorize(Roles = "admins")]
        [Route("{teacherId}/addstudent/{studentSubjectId:int}")]
        [ResponseType(typeof(AdminTeacherDTO))]
        public IHttpActionResult PutTeacherAddStudent(string teacherId, int studentSubjectId)
        {
            logger.Info("Requesting to add student to a teacher");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Teacher teacher = teacherService.GetById(teacherId);
            StudentSubjectMarks studentSubject = ssmService.GetById(studentSubjectId);

            if (teacher == null || studentSubject == null)
            {
                return NotFound();
            }

            TeacherSubject teacherSubject = tsService.GetByTeacherAndSubject(teacher, studentSubject.Subject);
            if (teacherSubject == null)
            {
                return BadRequest("That teacher is not teaching that subject.");
            }

            if (studentSubject.TeacherSubject == teacherSubject)
            {
                return BadRequest("That teacher already teaches that subject to that student.");
            }

            return Ok(new AdminTeacherDTO(teacherService.AddStudentSubject(teacherSubject, studentSubject)));
        }

        // PUT: api/teachers/5/removestudent/5
        // [Authorize(Roles = "admins")]
        [Route("{teacherId}/removestudent/{studentSubjectId:int}")]
        [ResponseType(typeof(AdminTeacherDTO))]
        public IHttpActionResult PutTeacherRemoveStudent(string teacherId, int studentSubjectId)
        {
            logger.Info("Requesting to remove student from a teacher");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Teacher teacher = teacherService.GetById(teacherId);
            StudentSubjectMarks studentSubject = ssmService.GetById(studentSubjectId);
            if (teacher == null || studentSubject == null)
            {
                return NotFound();
            }

            TeacherSubject teacherSubject = tsService.GetByTeacherAndSubject(teacher, studentSubject.Subject);
            if (teacherSubject == null || studentSubject.TeacherSubject != teacherSubject)
            {
                return BadRequest("That teacher is not teaching that subject to that student.");
            }

            return Ok(new AdminTeacherDTO(teacherService.RemoveStudentSubject(teacherSubject, studentSubject)));
        }

        // DELETE: api/teachers/5
        // [Authorize(Roles = "admins")]
        [Route("{id}")]
        [ResponseType(typeof(AdminTeacherDTO))]
        public IHttpActionResult DeleteTeacher(string id)
        {
            logger.Info("Requesting to delete a teacher");

            Teacher teacher = teacherService.GetById(id);
            if (teacher == null)
            {
                return NotFound();
            }

            return Ok(new AdminTeacherDTO(teacherService.Delete(teacher)));
        }
    }
}