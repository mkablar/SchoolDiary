﻿using NLog;
using SchoolDiary.Models;
using SchoolDiary.Models.DTOs.UpdateDTOs;
using SchoolDiary.Models.DTOs.UserDTOs;
using SchoolDiary.Models.DTOs.UserDTOs.ParentDTOs;
using SchoolDiary.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace SchoolDiary.Controllers
{
    [RoutePrefix("api/parents")]
    public class ParentController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private IParentService parentService;
        private IUserService userService;
        private IStudentService studentService;

        public ParentController(IParentService parentService, IUserService userService, IStudentService studentService)
        {
            this.parentService = parentService;
            this.studentService = studentService;
            this.userService = userService;
        }

        // GET: api/teachers
        // [Authorize]
        [Route("")]
        [ResponseType(typeof(PublicUserDTO))]
        public IEnumerable<PublicUserDTO> Get()
        {
            logger.Info("Requesting parents");

            return parentService.Get().Select(x => new PublicUserDTO(x));
        }

        // GET: api/teachers/5
        // [Authorize(Roles = "parents, students")]
        [Route("{id}")]
        [ResponseType(typeof(PublicParentDTO))]
        public IHttpActionResult GetByIdPublic(string id)
        {
            logger.Info("Parent or student requesting a parent");

            Parent parent = parentService.GetById(id);
            if (parent == null)
            {
                return NotFound();
            }

            string userId = ((ClaimsPrincipal)RequestContext.Principal).FindFirst(x => x.Type == "UserId").Value;
            if (studentService.CheckIfParent(id, studentService.GetById(userId)))
            {
                return Ok(new AdminParentDTO(parent));
            }

            return Ok(new PublicParentDTO(parent));
        }

        // GET: api/parents/admin/5
        // [Authorize(Roles = "admins")]
        [Route("admin/{id}")]
        [ResponseType(typeof(AdminParentDTO))]
        public IHttpActionResult GetByIdAdmin(string id)
        {
            logger.Info("Admin requesting a parent");

            Parent parent = parentService.GetById(id);
            if (parent == null)
            {
                return NotFound();
            }

            return Ok(new AdminParentDTO(parent));
        }

        // GET: api/parents/teacher/5
        // [Authorize(Roles = "teachers")]
        [Route("teacher/{id}")]
        [ResponseType(typeof(TeacherParentDTO))]
        public IHttpActionResult GetByIdTeacher(string id)
        {
            logger.Info("Teacher requesting a parent");

            Parent parent = parentService.GetById(id);
            if (parent == null)
            {
                return NotFound();
            }

            return Ok(new TeacherParentDTO(parent));
        }

        // PUT: api/teachers
        // [Authorize(Roles = "admins")]
        [Route("{id}")]
        [ResponseType(typeof(AdminParentDTO))]
        public IHttpActionResult PutParent(string id, UpdateParentDTO userModel)
        {
            logger.Info("Requesting to change a parent");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Parent parent = parentService.GetById(id);
            if (parent == null || parent.Id != userModel.Id)
            {
                return NotFound();
            }
            
            // if either the email or username have changed, we need to check if the new email and/or username are available
            if ((parent.UserName != userModel.UserName || parent.Email != userModel.Email) &&
                userService.UserInDb(userModel.UserName, out string message, userModel.Email))
            {
                return BadRequest(message);
            }

            return Ok(new AdminParentDTO(parentService.Update(parent, userModel)));
        }

        // DELETE: api/teachers
        // [Authorize(Roles = "admins")]
        [Route("{id}")]
        [ResponseType(typeof(AdminParentDTO))]
        public IHttpActionResult DeleteParent(string id)
        {
            logger.Info("Requesting to delete a parent");

            Parent parent = parentService.GetById(id);
            if (parent == null)
            {
                return NotFound();
            }

            return Ok(new AdminParentDTO(parentService.Delete(parent)));
        }
    }
}