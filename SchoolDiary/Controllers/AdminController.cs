﻿using NLog;
using SchoolDiary.Models;
using SchoolDiary.Models.DTOs.UpdateDTOs;
using SchoolDiary.Models.DTOs.UserDTOs;
using SchoolDiary.Models.DTOs.UserDTOs.AdminDTOs;
using SchoolDiary.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace SchoolDiary.Controllers
{
    [RoutePrefix("api/admins")]
    public class AdminController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private IAdminService adminService;
        private IUserService userService;

        public AdminController(IAdminService adminService, IUserService userService)
        {
            this.adminService = adminService;
            this.userService = userService;
        }

        // GET: api/admins
        // [Authorize]
        [Route("")]
        [ResponseType(typeof(PublicUserDTO))]
        public IEnumerable<PublicUserDTO> Get(string temp = null)
        {
            logger.Info("Requesting admins");

            if (temp == null || temp == "")
            {
                return adminService.Get().Select(x => new PublicUserDTO(x));
            } else
            {
                return adminService.Get().Where(x => $"{x.FirstName} {x.LastName}".ToLower().Contains(temp.ToLower())).Select(x => new PublicUserDTO(x));
            }

        }

        // GET: api/admins/5
        // [Authorize(Roles = "parents, students")]
        [Route("{id}")]
        [ResponseType(typeof(PublicAdminDTO))]
        public IHttpActionResult GetByIdPublic(string id)
        {
            logger.Info("Parent or student requesting admin");

            Admin admin = adminService.GetById(id);
            if (admin == null)
            {
                return NotFound();
            }

            return Ok(new PublicAdminDTO(admin));
        }

        // GET: api/admins/admin/5
        // [Authorize(Roles = "admins")]
        [Route("admin/{id}")]
        [ResponseType(typeof(AdminAdminDTO))]
        public IHttpActionResult GetByIdAdmin(string id)
        {
            logger.Info("Admin requesting admin");

            Admin admin = adminService.GetById(id);
            if (admin == null)
            {
                return NotFound();
            }

            return Ok(new AdminAdminDTO(admin));
        }

        // GET: api/admins/teacher/5
        // [Authorize(Roles = "teachers")]
        [Route("teacher/{id}")]
        [ResponseType(typeof(TeacherAdminDTO))]
        public IHttpActionResult GetByIdTeacher(string id)
        {
            logger.Info("Teacher requesting admin");

            Admin admin = adminService.GetById(id);
            if (admin == null)
            {
                return NotFound();
            }

            return Ok(new TeacherAdminDTO(admin));
        }

        // GET: api/admins/logs
        // [Authorize(Roles = "admins")]
        [Route("logs")]
        public IHttpActionResult GetLogs()
        {
            return Ok(adminService.GetLogs());
        }

        // PUT: api/admins/5
        // [Authorize(Roles = "admins")]
        [Route("{id}")]
        [ResponseType(typeof(AdminAdminDTO))]
        public IHttpActionResult PutAdmin(string id, UpdateAdminDTO userModel)
        {
            logger.Info("Requesting to change admin");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Admin admin = adminService.GetById(id);
            if (admin == null || userModel.Id != admin.Id)
            {
                return NotFound();
            }

            // if either the email or username have changed, we need to check if the new email and/or username are available
            if ((admin.UserName != userModel.UserName || admin.Email != userModel.Email) && 
                userService.UserInDb(userModel.UserName, out string message, userModel.Email))
            {
                return BadRequest(message);
            }

            return Ok(new AdminAdminDTO(adminService.Update(admin, userModel)));
        }

        // DELETE: api/adminl/5
        // [Authorize(Roles = "admins")]
        [Route("{id}")]
        [ResponseType(typeof(AdminAdminDTO))]
        public IHttpActionResult DeleteAdmin(string id)
        {
            logger.Info("Requesting to delete admin");

            Admin admin = adminService.GetById(id);
            if (admin == null)
            {
                return NotFound();
            }

            return Ok(new AdminAdminDTO(adminService.Delete(admin)));
        }
    }
}
