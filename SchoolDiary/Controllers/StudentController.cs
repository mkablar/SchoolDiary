﻿using NLog;
using SchoolDiary.Models;
using SchoolDiary.Models.DTOs;
using SchoolDiary.Models.DTOs.UpdateDTOs;
using SchoolDiary.Models.DTOs.UserDTOs;
using SchoolDiary.Models.DTOs.UserDTOs.StudentDTOs;
using SchoolDiary.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace SchoolDiary.Controllers
{
    [RoutePrefix("api/students")]
    public class StudentController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private IStudentService studentService;
        private IUserService userService;
        private IParentService parentService;
        private ISubjectService subjectService;
        private IStudentSubjectMarksService ssmService;
        private ITeacherSubjectService tsService;

        public StudentController(IStudentService studentService, IUserService userService, ISubjectService subjectService, IStudentSubjectMarksService ssmService, ITeacherSubjectService tsService, IParentService parentService)
        {
            this.studentService = studentService;
            this.userService = userService;
            this.subjectService = subjectService;
            this.ssmService = ssmService;
            this.tsService = tsService;
            this.parentService = parentService;
        }

        // GET: api/ students
        // [Authorize]
        [Route("")]
        [ResponseType(typeof(AdminStudentDTO))]
        public IEnumerable<PublicUserDTO> Get()
        {
            logger.Info("Requesting students");

            return studentService.Get().Select(x => new AdminStudentDTO(x));
        }

        // GET: api/students/5
        // [Authorize(Roles = "parents, students")]
        [Route("{id}")]
        [ResponseType(typeof(PublicStudentDTO))]
        public IHttpActionResult GetByIdPublic(string id)
        {
            logger.Info("Parent or student requesting student");

            Student student = studentService.GetById(id);
            if (student == null)
            {
                return NotFound();
            }

            string userId = ((ClaimsPrincipal)RequestContext.Principal).FindFirst(x => x.Type == "UserId").Value;
            if (studentService.CheckIfParent(userId, student))
            {
                return Ok(new AdminStudentDTO(student));
            }

            return Ok(new PublicStudentDTO(student));
        }

        // GET: api/students/admin/5
        // [Authorize(Roles = "admins")]
        [Route("admin/{id}")]
        [ResponseType(typeof(AdminStudentDTO))]
        public IHttpActionResult GetByIdAdmin(string id)
        {
            logger.Info("Admin requesting student");

            Student student = studentService.GetById(id);
            if (student == null)
            {
                return NotFound();
            }

            return Ok(new AdminStudentDTO(student));
        }

        // GET: api/students/teacher/5
        // [Authorize(Roles = "teachers")]
        [Route("teacher/{id}")]
        [ResponseType(typeof(PublicStudentDTO))]
        public IHttpActionResult GetByIdTeacher(string id)
        {
            logger.Info("Teacher requesting student");

            Student student = studentService.GetById(id);
            if (student == null)
            {
                return NotFound();
            }

            string userId = ((ClaimsPrincipal)RequestContext.Principal).FindFirst(x => x.Type == "UserId").Value;
            if (studentService.CheckIfTeacher(userId, student))
            {
                return Ok(new AdminStudentDTO(student));
            }

            return Ok(new PublicStudentDTO(student));
        }

        // GET: api/students/5/getmarks
        // [Authorize(Roles = "admins, teachers, parents")]
        [Route("{id}/getmarks")]
        [ResponseType(typeof(PublicStudentSubjectMarksDTO))]
        public IHttpActionResult GetStudentMarks(string id)
        {
            logger.Info("Requesting marks for a student");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Student student = studentService.GetById(id);
            if (student == null)
            {
                return NotFound();
            }

            // string userId = ((ClaimsPrincipal)RequestContext.Principal).FindFirst(x => x.Type == "UserId").Value;
            // if (!RequestContext.Principal.IsInRole("admins") && !studentService.CheckIfParent(userId, student) && !studentService.CheckIfTeacher(userId, student))
            // {
            //     return BadRequest("You do not have the authority to view this content");
            // }

            return Ok(studentService.GetMarks(student).Select(x => new PublicStudentSubjectMarksDTO(x)));
        }

        // PUT: api/students
        // [Authorize(Roles = "admins")]
        [Route("{id}")]
        [ResponseType(typeof(AdminStudentDTO))]
        public IHttpActionResult PutStudent(string id, UpdateStudentDTO userModel)
        {
            logger.Info("Requesting to change student");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Student student = studentService.GetById(id);
            if (student == null || student.Id != userModel.Id)
            {
                return NotFound();
            }

            // if username has changed, we need to check if the new username is available
            if ((student.UserName != userModel.UserName) && userService.UserInDb(userModel.UserName, out string message))
            {
                return BadRequest(message);
            }

            return Ok(new AdminStudentDTO(studentService.Update(student, userModel)));
        }

        // PUT: api/students/5/addparent/5
        // [Authorize(Roles = "admins")]
        [Route("{studentId}/addparent/{parentId}")]
        [ResponseType(typeof(AdminStudentDTO))]
        public IHttpActionResult PutStudentAddParent(string studentId, string parentId)
        {
            logger.Info("Requesting to add parent to a student");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Student student = studentService.GetById(studentId);
            Parent parent = parentService.GetById(parentId);
            if (student == null || parent == null)
            {
                return NotFound();
            }

            if (student.Parents.Count == 2)
            {
                return BadRequest("There is already 2 parents associated with this student.");
            }

            return Ok(new AdminStudentDTO(studentService.AddParent(student, parent)));
        }

        // PUT: api/students/5/removeparent/5
        // [Authorize(Roles = "admins")]
        [Route("{stuentId}/removeparent/{parentId}")]
        [ResponseType(typeof(AdminStudentDTO))]
        public IHttpActionResult PutStudentRemoveParent(string studentId, string parentId)
        {
            logger.Info("Requesting to remove a parent from a student");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Student student = studentService.GetById(studentId);
            Parent parent = parentService.GetById(parentId);
            if (student == null || parent == null)
            {
                return NotFound();
            }

            if (!student.Parents.Contains(parent))
            {
                return BadRequest("That parent is not associated with that student.");
            }
            
            return Ok(new AdminStudentDTO(studentService.RemoveParent(student, parent)));
        }

        // PUT: api/students/5/addsubject/5
        // [Authorize(Roles = "admins")]
        [Route("{studentId}/addsubject/{subjectId:int}")]
        [ResponseType(typeof(AdminStudentDTO))]
        public IHttpActionResult PutStudentAddSubject(string studentId, int subjectId)
        {
            logger.Info("Requesting to add subject to a student");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Student student = studentService.GetById(studentId);
            Subject subject = subjectService.GetById(subjectId);

            if (student == null || subject == null)
            {
                return NotFound();
            }

            if (ssmService.GetByStudentAndSubject(student, subject) != null)
            {
                return BadRequest("That student already attends that subject.");
            }

            return Ok(new AdminStudentDTO(studentService.AddSubject(student, subject)));
        }

        // PUT: api/students/5/removesubject/5
        // [Authorize(Roles = "admins")]
        [Route("{studentId}/removesubject/{subjectId:int}")]
        [ResponseType(typeof(AdminStudentDTO))]
        public IHttpActionResult PutStudentRemoveSubject(string studentId, int subjectId)
        {
            logger.Info("Requesting to remove subject from a student");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Student student = studentService.GetById(studentId);
            Subject subject = subjectService.GetById(subjectId);

            if (student == null || subject == null)
            {
                return NotFound();
            }

            StudentSubjectMarks studentSubject = ssmService.GetByStudentAndSubject(student, subject);
            if (studentSubject == null)
            {
                return BadRequest("That student does not attend that subject.");
            }

            return Ok(new AdminStudentDTO(studentService.RemoveSubject(studentSubject)));
        }

        // PUT: api/students/5/addteacher/5
        // [Authorize(Roles = "admins")]
        [Route("{studentId}/addteacher/{teacherSubjectId:int}")]
        [ResponseType(typeof(AdminStudentDTO))]
        public IHttpActionResult PutStudentAddTeacher(string studentId, int teacherSubjectId)
        {
            logger.Info("Requesting to add teacher to a student");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Student student = studentService.GetById(studentId);
            TeacherSubject teacherSubject = tsService.GetById(teacherSubjectId);

            if (student == null || teacherSubject == null)
            {
                return NotFound();
            }

            StudentSubjectMarks studentSubject = ssmService.GetByStudentAndSubject(student, teacherSubject.Subject);
            if (studentSubject == null)
            {
                return BadRequest("That student is not attending that subject.");
            }

            if (studentSubject.TeacherSubject == teacherSubject)
            {
                return BadRequest("That students already attends that subject with that teacher.");
            }

            return Ok(new AdminStudentDTO(studentService.AddTeacherSubject(studentSubject, teacherSubject)));
        }

        // PUT: api/students/5/removeteacher/5
        // [Authorize(Roles = "admins")]
        [Route("{studentId}/removeteacher/{teacherSubjectId:int}")]
        [ResponseType(typeof(AdminStudentDTO))]
        public IHttpActionResult PutStudentRemoveTeacher(string studentId, int teacherSubjectId)
        {
            logger.Info("Requesting to remove a teacher from a student");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Student student = studentService.GetById(studentId);
            TeacherSubject teacherSubject = tsService.GetById(teacherSubjectId);

            if (student == null || teacherSubject == null)
            {
                return NotFound();
            }

            StudentSubjectMarks studentSubject = ssmService.GetByStudentAndSubject(student, teacherSubject.Subject);
            if (studentSubject == null || studentSubject.TeacherSubject != teacherSubject)
            {
                return BadRequest("That student is not attending that subject with that teacher.");
            }

            return Ok(new AdminStudentDTO(studentService.RemoveTeacherSubject(studentSubject, teacherSubject)));
        }

        // PUT: api/students/5/givemark/5
        // [Authorize(Roles = "admins, teachers")]
        [Route("{studentId}/givemark/{subjectId:int}")]
        [ResponseType(typeof(AdminStudentDTO))]
        public IHttpActionResult PutStudentGiveMark(string studentId, int subjectId, MarkDTO markModel)
        {
            logger.Info("Requesting to give a mark to a student");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Student student = studentService.GetById(studentId);
            Subject subject = subjectService.GetById(subjectId);
            StudentSubjectMarks studentSubject = ssmService.GetByStudentAndSubject(student, subject);
            if (student == null || subject == null || studentSubject == null)
            {
                return NotFound();
            }

            ApplicationUser grader = userService.GetById(((ClaimsPrincipal)RequestContext.Principal).FindFirst(x => x.Type == "UserId").Value);
            if (!RequestContext.Principal.IsInRole("admins") && !(studentSubject.TeacherSubject.Teacher == grader))
            {
                return BadRequest("You do not have the authority to give a mark to this student for this subject.");
            }

            return Ok(new AdminStudentDTO(studentService.GiveMark(grader, studentSubject, markModel)));
        }

        // DELETE: api/students/5
        // [Authorize(Roles = "admins")]
        [Route("{id}")]
        [ResponseType(typeof(AdminStudentDTO))]
        public IHttpActionResult DeleteStudent(string id)
        {
            logger.Info("Requesting to delete a student");

            Student student = studentService.GetById(id);
            if (student == null)
            {
                return NotFound();
            }

            return Ok(new AdminStudentDTO(studentService.Delete(student)));
        }
    }
}