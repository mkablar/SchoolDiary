﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using SchoolDiary.Models;
using SchoolDiary.Models.DTOs;
using SchoolDiary.Repositories;
using SchoolDiary.Services;
using SchoolDiary.Models.DTOs.UserDTOs;
using System.Web.Http.Description;
using NLog;

namespace SchoolDiary.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private IUserService userService;

        public AccountController(IUserService userService)
        {
            this.userService = userService;
        }

        // GET: api/account
        // [Authorize(Roles = "admins")]
        [ResponseType(typeof(ApplicationUser))]
        [Route("")]
        public IEnumerable<ApplicationUser> Get()
        {
            logger.Info("Requestng application users");

            return userService.Get();
        }

        // GET: api/acconunt/5
        // [Authorize(Roles = "admins")]
        [ResponseType(typeof(ApplicationUser))]
        [Route("{id}")]
        public IHttpActionResult GetById(string id)
        {
            logger.Info("Requesting an apllication user");

            ApplicationUser user = userService.GetById(id);
            if (user == null)
            {
                return NotFound();
            }


            return Ok(user);
        }

        // GET: api/account/names
        // [Authorize(Roles = "admins")]
        [ResponseType(typeof(ApplicationUser))]
        [Route("names")]
        public IHttpActionResult GetByFirstLastName([FromUri]string firstName, [FromUri]string lastName)
        {
            logger.Info("Requesting a user by his first and last name");

            // treba napisati kod
            return Ok();
        }

        // POST: api/account/register-admin
        // [Authorize(Roles = "admins")]
        [Route("register-admin")]
        public async Task<IHttpActionResult> PostAdmin(AdminUserDTO userModel)
        {
            logger.Info("Requesting to insert an admin");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (userService.UserInDb(userModel.UserName, out string message, userModel.Email))
            {
                return BadRequest(message);
            }

            var result = await userService.RegisterAdmin(userModel);

            if (result == null)
            {
                return BadRequest(ModelState);
            }

            return Ok();
        }

        // POST: api/account/register-teacher
        // [Authorize(Roles = "admins")]
        [Route("register-teacher")]
        public async Task<IHttpActionResult> PostTeacher(TeacherUserDTO userModel)
        {
            logger.Info("Requesting to insert a teacher");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (userService.UserInDb(userModel.UserName, out string message, userModel.Email))
            {
                return BadRequest(message);
            }

            var result = await userService.RegisterTeacher(userModel);

            if (result == null)
            {
                return BadRequest(ModelState);
            }

            return Ok();
        }

        // POST: api/account/register-parent
        // [Authorize(Roles = "admins")]
        [Route("register-parent")]
        public async Task<IHttpActionResult> PostParent(ParentUserDTO userModel)
        {
            logger.Info("Requesting to insert a parent");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (userService.UserInDb(userModel.UserName, out string message, userModel.Email))
            {
                return BadRequest(message);
            }

            var result = await userService.RegisterParent(userModel);

            if (result == null)
            {
                return BadRequest(ModelState);
            }

            return Ok();
        }

        // POST: api/account/register-student
        // [Authorize(Roles = "admins")]
        [Route("register-student")]
        public async Task<IHttpActionResult> PostStudent(StudentUserDTO userModel)
        {
            logger.Info("Requesting to insert a student");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (userService.UserInDb(userModel.UserName, out string message))
            {
                return BadRequest(message);
            }

            var result = await userService.RegisterStudent(userModel);

            if (result == null)
            {
                return BadRequest(ModelState);
            }

            return Ok();
        }

        // PUT: api/account/5/change-password
        // [Authorize(Roles = "admins")]
        // [Route("{id}/change-password")]
        // public async Task<IHttpActionResult> PutChangePassword(string password, string confirmPassword)
        // {
        //     logger.Info("Requesting to change the password");

        //     if (!ModelState.IsValid)
        //     {
        //         return BadRequest(ModelState);
        //     }

        //     if (password != confirmPassword)
        //     {
        //         return BadRequest("Passwords do not match.");
        //     }
        // }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
