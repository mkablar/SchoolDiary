﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolDiary.Models;

namespace SchoolDiary.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<ApplicationUser> UsersRepository { get; }
        IGenericRepository<Admin> AdminsRepository { get; }
        IGenericRepository<Teacher> TeachersRepository { get; }
        IGenericRepository<Parent> ParentsRepository { get; }
        IGenericRepository<Student> StudentsRepository { get; }
        IGenericRepository<Subject> SubjectsRepository { get; }
        IGenericRepository<TeacherSubject> TeachersSubjectsRepository { get; }
        IGenericRepository<StudentSubjectMarks> StudentsSubjectsMarksRepository { get; }
        IGenericRepository<Mark> MarksRepository { get; }
        IAuthRepository AuthRepository { get; }

        void Save();
    }
}
