﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using SchoolDiary.Models;

namespace SchoolDiary.Repositories
{
    public interface IAuthRepository : IDisposable
    {
        Task<IList<string>> FindRoles(string userId);
        Task<ApplicationUser> FindUser(string userName, string password);
        Task<IdentityResult> RegisterAdminUser(Admin userModel, string password);
        Task<IdentityResult> RegisterTeacherUser(Teacher userModel, string password);
        Task<IdentityResult> RegisterStudentUser(Student userModel, string password);
        Task<IdentityResult> RegisterParentUser(Parent userModel, string password);
    }
}