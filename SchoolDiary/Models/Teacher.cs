﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models
{
    public class Teacher : ApplicationUser
    {
        public virtual ICollection<TeacherSubject> TeacherSubjects { get; set; }

        public Teacher()
        {
            TeacherSubjects = new List<TeacherSubject>();
        }
    }
}