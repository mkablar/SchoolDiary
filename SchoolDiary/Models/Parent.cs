﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models
{
    public class Parent : ApplicationUser
    {
        [JsonIgnore]
        public virtual ICollection<Student> Kids { get; set; }

        public Parent()
        {
            Kids = new List<Student>();
        }
    }
}