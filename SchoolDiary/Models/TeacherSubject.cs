﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models
{
    public class TeacherSubject
    {
        public int Id { get; set; }

        [ForeignKey("Teacher")]
        public string TeacherId { get; set; }

        [ForeignKey("TeacherId")]
        public virtual Teacher Teacher { get; set; }

        [ForeignKey("Subject")]
        public int? SubjectId { get; set; }

        [ForeignKey("SubjectId")]
        public virtual Subject Subject { get; set; }

        public virtual ICollection<StudentSubjectMarks> StudentSubjectMarks { get; set; }

        public TeacherSubject()
        {
            StudentSubjectMarks = new List<StudentSubjectMarks>();
        }
    }
}