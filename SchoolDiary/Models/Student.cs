﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models
{
    public class Student : ApplicationUser
    {
        [JsonIgnore]
        public virtual ICollection<Parent> Parents { get; set; }
        public virtual ICollection<StudentSubjectMarks> StudentSubjectMarks { get; set; }

        public Student()
        {
            Parents = new List<Parent>();
            StudentSubjectMarks = new List<StudentSubjectMarks>();
        }
    }
}