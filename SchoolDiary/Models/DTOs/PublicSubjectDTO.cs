﻿using SchoolDiary.Models.DTOs.UserDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs
{
    public class PublicSubjectDTO : AnonymousSubjectDTO
    {
        public string Description { get; set; }
        public int WeeklySubjectFund { get; set; }
        public ICollection<PublicUserDTO> Teachers { get; set; }

        public PublicSubjectDTO() { }

        public PublicSubjectDTO(Subject subject) : base(subject)
        {
            WeeklySubjectFund = subject.WeeklySubjectFund;
            Description = subject.Description;
            Teachers = subject.TeacherSubjects.Select(x => new PublicUserDTO(x.Teacher)).ToList();
        }
    }
}