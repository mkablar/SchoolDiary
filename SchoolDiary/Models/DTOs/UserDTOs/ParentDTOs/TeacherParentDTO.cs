﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs.UserDTOs.ParentDTOs
{
    public class TeacherParentDTO : PublicParentDTO
    {
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public TeacherParentDTO()
        {

        }

        public TeacherParentDTO(Parent parent) : base(parent)
        {
            PhoneNumber = parent.PhoneNumber;
            Email = parent.Email;
        }
    }
}