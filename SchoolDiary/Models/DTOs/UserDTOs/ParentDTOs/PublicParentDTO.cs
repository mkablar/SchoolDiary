﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs.UserDTOs.ParentDTOs
{
    public class PublicParentDTO : PublicUserDTO
    {
        public ICollection<PublicUserDTO> Kids { get; set; }

        public PublicParentDTO()
        {

        }

        public PublicParentDTO(Parent parent) : base(parent)
        {
            Kids = parent.Kids.Select(x => new PublicUserDTO(x)).ToList();
        }
    }
}