﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs.UserDTOs.ParentDTOs
{
    public class AdminParentDTO : TeacherParentDTO
    {
        public string Address { get; set; }

        public string UserName { get; set; }

        public AdminParentDTO()
        {
            
        }

        public AdminParentDTO(Parent parent) : base(parent)
        {
            UserName = parent.UserName;
            Address = parent.Address;
        }
    }
}