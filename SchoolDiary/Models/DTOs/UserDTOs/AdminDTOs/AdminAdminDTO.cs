﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs.UserDTOs.AdminDTOs
{
    public class AdminAdminDTO : TeacherAdminDTO
    {
        public string Address { get; set; }

        public string UserName { get; set; }

        public AdminAdminDTO() { }

        public AdminAdminDTO(Admin admin) : base(admin)
        {
            Address = admin.Address;
            UserName = admin.UserName;
        }
    }
}