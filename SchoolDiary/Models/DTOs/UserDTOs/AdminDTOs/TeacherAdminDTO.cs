﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs.UserDTOs.AdminDTOs
{
    public class TeacherAdminDTO : PublicAdminDTO
    {
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public TeacherAdminDTO() { }

        public TeacherAdminDTO(Admin admin) : base(admin)
        {
            PhoneNumber = admin.PhoneNumber;
            Email = admin.Email;
        }
    }
}