﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs.UserDTOs.AdminDTOs
{
    public class PublicAdminDTO : PublicUserDTO
    {
        public PublicAdminDTO() { }

        public PublicAdminDTO(Admin admin) : base(admin)
        {
        }
    }
}