﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs.UserDTOs.TeacherDTOs
{
    public class TeacherTeacherDTO : PublicTeacherDTO
    {
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public TeacherTeacherDTO() { }

        public TeacherTeacherDTO(Teacher teacher) : base(teacher)
        {
            PhoneNumber = teacher.PhoneNumber;
            Email = teacher.Email;
        }
    }
}