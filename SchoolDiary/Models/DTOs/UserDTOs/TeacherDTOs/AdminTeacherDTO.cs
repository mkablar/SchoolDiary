﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs.UserDTOs.TeacherDTOs
{
    public class AdminTeacherDTO : TeacherTeacherDTO
    {
        public string Address { get; set; }
        public string UserName { get; set; }

        public AdminTeacherDTO() { }

        public AdminTeacherDTO(Teacher teacher) : base(teacher)
        {
            UserName = teacher.UserName;
            Address = teacher.Address;
        }
    }
}