﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs.UserDTOs.TeacherDTOs
{
    public class PublicTeacherDTO : PublicUserDTO
    {
        public ICollection<AnonymousSubjectDTO> Subjects { get; set; }

        public PublicTeacherDTO() { }

        public PublicTeacherDTO(Teacher teacher) : base(teacher)
        {
            Subjects = teacher.TeacherSubjects.Select(x => new AnonymousSubjectDTO(x.Subject)).ToList();
        }
    }
}