﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs.UserDTOs.StudentDTOs
{
    public class AdminStudentDTO : TeacherStudentDTO
    {
        public string Address { get; set; }
        public string UserName { get; set; }
        ICollection<PublicStudentSubjectMarksDTO> StudentSubject { get; set; }

        public AdminStudentDTO()
        {
        }

        public AdminStudentDTO(Student student) : base(student)
        {
            UserName = student.UserName;
            Address = student.Address;
            StudentSubject = student.StudentSubjectMarks.Select(x => new PublicStudentSubjectMarksDTO(x)).ToList();
        }
    }
}