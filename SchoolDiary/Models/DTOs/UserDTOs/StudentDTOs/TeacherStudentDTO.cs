﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs.UserDTOs.StudentDTOs
{
    public class TeacherStudentDTO : PublicStudentDTO
    {
        public TeacherStudentDTO()
        {

        }

        public TeacherStudentDTO(Student student) : base(student)
        {
        }
    }
}