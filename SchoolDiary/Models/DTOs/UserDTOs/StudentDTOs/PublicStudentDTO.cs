﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs.UserDTOs.StudentDTOs
{
    public class PublicStudentDTO : PublicUserDTO
    {
        public ICollection<PublicUserDTO> Parents { get; set; }

        public PublicStudentDTO() { }

        public PublicStudentDTO(Student student) : base(student)
        {
            Parents = student.Parents.Select(x => new PublicUserDTO(x)).ToList();
        }
    }
}