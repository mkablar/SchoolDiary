﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs
{
    public class AnonymousSubjectDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public AnonymousSubjectDTO() { }

        public AnonymousSubjectDTO(Subject subject)
        {
            Id = subject.Id;
            Name = subject.Name;
        }
    }
}