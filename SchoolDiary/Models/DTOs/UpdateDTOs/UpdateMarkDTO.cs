﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs.UpdateDTOs
{
    public class UpdateMarkDTO
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [Range(1, 5, ErrorMessage = "Value of the mark must be between 1 and 5")]
        public int Value { get; set; }

        [Required]
        public string Description { get; set; }
    }
}