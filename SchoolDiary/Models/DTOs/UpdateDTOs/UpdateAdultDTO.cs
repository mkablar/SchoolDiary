﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs.UpdateDTOs
{
    public class UpdateAdultDTO : UpdateUserDTO
    {
        [Required]
        public string PhoneNumber { get; set; }

        [Required]
        public string Email { get; set; }
    }
}