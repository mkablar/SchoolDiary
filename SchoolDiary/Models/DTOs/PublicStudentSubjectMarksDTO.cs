﻿using SchoolDiary.Models.DTOs.UserDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs
{
    public class PublicStudentSubjectMarksDTO
    {
        public int Id { get; set; }
        public PublicUserDTO Student { get; set; }
        public PublicUserDTO Teacher { get; set; }
        public AnonymousSubjectDTO Subject { get; set; }
        public ICollection<PublicMarkDTO> Marks { get; set; }

        public PublicStudentSubjectMarksDTO() { }

        public PublicStudentSubjectMarksDTO(StudentSubjectMarks studentSubject)
        {
            Id = studentSubject.Id;
            Student = new PublicUserDTO(studentSubject.Student);
            Teacher = new PublicUserDTO(studentSubject.TeacherSubject.Teacher);
            Subject = new AnonymousSubjectDTO(studentSubject.Subject);
            Marks =  studentSubject.Marks.Select(x => new PublicMarkDTO(x)).ToList();
        }
    }
}