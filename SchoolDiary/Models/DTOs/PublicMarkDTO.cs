﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs
{
    public class PublicMarkDTO
    {
        public int Id { get; set; }
        public int Value { get; set; }

        public PublicMarkDTO(Mark mark)
        {
            Id = mark.Id;
            Value = mark.Value;
        }
    }
}