﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs
{
    public class SubjectDTO
    {
        [Required]
        [StringLength(30, ErrorMessage = "The {0} must be between {2} and {1} characters long.", MinimumLength = 3)]
        [Display(Name = "Subject name")]
        public string Name { get; set; }

        [Required]
        [StringLength(250, ErrorMessage = "The {0} must be less than {1} characters long.")]
        [Display(Name = "Subject description")]
        public string Description { get; set; }

        [Required]
        [Range(2, 5, ErrorMessage = "Weekly subject fund must be between 2 and 5")]
        public int WeeklySubjectFund { get; set; }
    }
}