﻿using SchoolDiary.Models.DTOs.UserDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models.DTOs
{
    public class AdminMarkDTO : PublicMarkDTO
    {
        public string Description { get; set; }
        public DateTime MarkDate { get; set; }
        public PublicUserDTO Student { get; set; }
        public PublicUserDTO Grader { get; set; }
        public AnonymousSubjectDTO Subject { get; set; }

        public AdminMarkDTO(Mark mark) : base(mark)
        {
            Description = mark.Description;
            MarkDate = mark.MarkDate;
            Student = new PublicUserDTO(mark.StudentSubjectMarks.Student);
            Grader = new PublicUserDTO(mark.Grader);
            Subject = new AnonymousSubjectDTO(mark.StudentSubjectMarks.Subject);
        }
    }
}