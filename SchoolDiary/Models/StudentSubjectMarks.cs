﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models
{
    public class StudentSubjectMarks
    {
        public int Id { get; set; }

        [ForeignKey("Student")]
        public string StudentId { get; set; }

        [ForeignKey("StudentId")]
        public virtual Student Student { get; set; }

        [ForeignKey("Subject")]
        public int SubjectId { get; set; }

        [ForeignKey("SubjectId")]
        public virtual Subject Subject { get; set; }

        [ForeignKey("TeacherSubject")]
        public int? TeacherSubjectId { get; set; }

        [ForeignKey("TeacherSubjectId")]
        public virtual TeacherSubject TeacherSubject { get; set; }

        [ForeignKey("FinalMark")]
        public int? FinalMarkId { get; set; }

        [ForeignKey("FinalMarkId")]
        public virtual Mark FinalMark { get; set; }

        public virtual ICollection<Mark> Marks { get; set; }

        public StudentSubjectMarks()
        {
            Marks = new List<Mark>();
        }
    }
}