﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models
{
    public class Mark
    {
        public int Id { get; set; }
        public int Value { get; set; }
        public string Description { get; set; }
        public DateTime MarkDate { get; set; }

        [ForeignKey("GraderId")]
        public virtual ApplicationUser Grader { get; set; }

        [ForeignKey("Grader")]
        public string GraderId { get; set; }

        [ForeignKey("StudentSubjectMarksId")]
        public virtual StudentSubjectMarks StudentSubjectMarks { get; set; }

        [ForeignKey("StudentSubjectMarks")]
        public int? StudentSubjectMarksId { get; set; }
    }
}