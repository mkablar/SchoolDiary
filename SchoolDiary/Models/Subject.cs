﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Models
{
    public class Subject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int WeeklySubjectFund { get; set; }

        public virtual ICollection<TeacherSubject> TeacherSubjects { get; set; }
        public virtual ICollection<StudentSubjectMarks> StudentSubjectMarks { get; set; }

        public Subject()
        {
            TeacherSubjects = new List<TeacherSubject>();
            StudentSubjectMarks = new List<StudentSubjectMarks>();
        }
    }
}