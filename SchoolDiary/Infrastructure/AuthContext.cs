﻿using Microsoft.AspNet.Identity.EntityFramework;
using SchoolDiary.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SchoolDiary.Infrastructure
{
    public class AuthContext : IdentityDbContext<ApplicationUser>
    {
        public AuthContext() : base("DataAccessConnection")
        {
            Database.SetInitializer(new AuthContextSeedInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Admin>().ToTable("Administrators");
            modelBuilder.Entity<Parent>().ToTable("Parents");
            modelBuilder.Entity<Teacher>().ToTable("Teachers");
            modelBuilder.Entity<Student>().ToTable("Students");
            modelBuilder.Entity<Subject>().ToTable("Subjects");
            modelBuilder.Entity<TeacherSubject>().ToTable("TeachersSubjects");
            modelBuilder.Entity<StudentSubjectMarks>().ToTable("StudentsSubjects");
            modelBuilder.Entity<Mark>().ToTable("Marks");
        }
        // public DbSet<Admin> Admins { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<TeacherSubject> TeacherSubjects { get; set; }
        public DbSet<StudentSubjectMarks> StudentSubjects { get; set; }
        public DbSet<Mark> Marks { get; set; }
    }
}