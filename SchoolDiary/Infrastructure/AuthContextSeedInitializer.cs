﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SchoolDiary.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SchoolDiary.Infrastructure
{
    public enum FirstNames { Milan, Nenad, Marko, Nikola, Veselin, Natasa, Doris, Nikolina, Zdenka, Danijela, Goran, Nemanja, Jovana }
    public enum LastNames { Miljanic, Stojakovic, Djordjevic, Grujic, Markovic, Petrovic, Nikolic, Jovanovic, Nenadic, Stankovic }
    public enum Addresses { Bulevar_Evrope, Bulevar_Oslobodjenja, Strazilovska, Gagarinova, Tolstojeva, Dostojevskog, Vuka_Karadzica }
    public enum Subjects { Matematika, Fizika, Srpski_Jezik, Engleski_Jezik, Hemija, Biologija, Istorija, Geografija }

    public class AuthContextSeedInitializer : DropCreateDatabaseIfModelChanges<AuthContext>
    {
        public Random rnd = new Random();
        private static int userCount = 50; // must be even number
        private static int userAdminCount = 2;
        private static int subjectCount = Enum.GetValues(typeof(Subjects)).Length;
        private static int userTeacherCount = subjectCount * 2;
        private static int studentCount = (userCount - userAdminCount - userTeacherCount) / 2;
        // admin + teacher count must be an even number
        private static string role;
        ApplicationUser[] users = new ApplicationUser[userCount];
        Student[] students = new Student[studentCount];
        Subject[] subjects = new Subject[subjectCount];
        TeacherSubject[] teacherSubjects = new TeacherSubject[userTeacherCount];
        // every student has every subject, so the array has the legth studentCount * subjectCount
        StudentSubjectMarks[] studentSubjectMarks = new StudentSubjectMarks[studentCount * subjectCount];

        private static int carrier;

        private Admin CreateAdmin(int i)
        {
            Admin admin = new Admin();
            admin = (Admin)CreateAdultUser(admin, i);

            return admin;
        }

        private Teacher CreateTeacher(int i)
        {
            Teacher teacher = new Teacher();
            teacher = (Teacher)CreateAdultUser(teacher, i);
            return teacher;
        }

        private Student CreateStudent(int i)
        {
            Student student = new Student();
            student = (Student)CreateUser(student, i);
            return student;
        }

        private Parent CreateParent(int i)
        {
            Parent parent = new Parent();
            parent = (Parent)CreateAdultUser(parent, i);

            return parent;
        }

        private ApplicationUser CreateAdultUser(ApplicationUser user, int i)
        {
            user = CreateUser(user, i);
            user.Email = $"{user.UserName}@example.com";
            carrier = rnd.Next(9); // will be 0-8
            carrier = carrier < 7 ? carrier : ++carrier; // if it is 0-6 we leave it, if it is 7 or 8 it becomes 8 or 9
            // we will generate only 7 digit mobile numbers, because it is more frequent
            user.PhoneNumber = $"+381 (6{carrier}) {rnd.Next(999).ToString("D3")} - {rnd.Next(9999).ToString("D4")}";
            return user;
        }

        private ApplicationUser CreateUser(ApplicationUser user, int i)
        {
            user.FirstName = Enum.GetName(typeof(FirstNames), rnd.Next(Enum.GetValues(typeof(FirstNames)).Length));
            user.LastName = Enum.GetName(typeof(LastNames), rnd.Next(Enum.GetValues(typeof(LastNames)).Length));
            user.UserName = $"{user.FirstName.ToLower()}.{user.LastName.ToLower()}{i + 1}";
            user.Address = $"{Enum.GetName(typeof(Addresses), rnd.Next(Enum.GetValues(typeof(Addresses)).Length)).Replace('_', ' ')} {rnd.Next(150) + 1}";

            return user;
        }
        protected override void Seed(AuthContext context)
        {
            // Add roles
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            RoleManager.Create(new IdentityRole("admins"));
            RoleManager.Create(new IdentityRole("teachers"));
            RoleManager.Create(new IdentityRole("parents"));
            RoleManager.Create(new IdentityRole("students"));

            context.SaveChanges();

            // Add users
            for (int i = 0, j = 0; i < userCount; i++)
            {
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                ApplicationUser user;
                if (i < userAdminCount + userTeacherCount)
                {
                    if (i < userAdminCount)
                    {
                        user = CreateAdmin(i);
                        role = "admins";
                    } else
                    {
                        user = CreateTeacher(i);
                        role = "teachers";
                    }
                } else
                {
                    if (i % 2 == 0)
                    {
                        user = CreateStudent(i);
                        role = "students";
                        students[j] = (Student)user;
                        j++;
                    } else
                    {
                        user = CreateParent(i);
                        role = "parents";
                    }
                }

                string password = "123456";
                users[i] = user;
                userManager.Create(user, password);
                userManager.AddToRole(user.Id, role);
            }
            context.SaveChanges();

            for (int i = userAdminCount + userTeacherCount + 1; i < userCount; i += 2)
            {
                Parent parent = (Parent)users[i];
                parent.LastName = users[i - 1].LastName;
                parent.Address = users[i - 1].Address;
                parent.Kids.Add((Student)users[i - 1]);
            }
            context.SaveChanges();

            for (int i = 0; i < subjectCount; i++)
            {
                Subject subject = new Subject();
                subject.Name = (Enum.GetName(typeof(Subjects), i)).Replace('_', ' ');
                subject.Description = $"Ucicemo gradivo iz predmeta {subject.Name}";
                subject.WeeklySubjectFund = rnd.Next(4) + 2;

                context.Subjects.Add(subject);
                subjects[i] = subject;
            }
            context.SaveChanges();

            for (int i = 0, j = userAdminCount; i < subjectCount; i++, j++)
            {
                TeacherSubject teacherSubject = new TeacherSubject
                {
                    Teacher = (Teacher)users[j],
                    Subject = subjects[i]
                };

                context.TeacherSubjects.Add(teacherSubject);

                TeacherSubject teacherSubject1 = new TeacherSubject
                {
                    Teacher = (Teacher)users[j + subjectCount],
                    Subject = subjects[i]
                };

                context.TeacherSubjects.Add(teacherSubject1);

                teacherSubjects[i] = teacherSubject;
                teacherSubjects[i + subjectCount] = teacherSubject1;
            }
            context.SaveChanges();

            for (int i = 0; i < students.Length; i++)
            {
                for (int j = 0; j < subjectCount; j++)
                {
                    Student student = students[i];
                    Subject subject = subjects[j];
                    TeacherSubject[] temp = teacherSubjects.Where(x => x.Subject == subject).ToArray();
                    StudentSubjectMarks studentSubject = new StudentSubjectMarks
                    {
                        Student = student,
                        Subject = subject,
                        TeacherSubject = temp[rnd.Next(temp.Length)]
                    };
                    // for (int e = 0; i < rnd.Next(7); e++)
                    // {
                    //     Mark mark = new Mark
                    //     {
                    //         Grader = studentSubject.TeacherSubject.Teacher,
                    //         StudentSubjectMarks = studentSubject,
                    //         Value = rnd.Next(5) + 1
                    //     };
                    //     mark.Description = $"User got the grade {mark.Value} for displayed knowledge.";
                    //     mark.MarkDate = DateTime.UtcNow;

                    //     studentSubject.Marks.Add(mark);
                    //     context.Marks.Add(mark);
                    // }
                    studentSubjectMarks[(subjectCount * i) + j] = studentSubject;
                    context.StudentSubjects.Add(studentSubject);
                }
            }
            context.SaveChanges();

            
            context.SaveChanges();

            base.Seed(context);
        }
    }
}