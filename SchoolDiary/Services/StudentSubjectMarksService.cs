﻿using SchoolDiary.Models;
using SchoolDiary.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Services
{
    public class StudentSubjectMarksService : IStudentSubjectMarksService
    {
        private IUnitOfWork db;
        public StudentSubjectMarksService(IUnitOfWork db)
        {
            this.db = db;
        }

        public IEnumerable<StudentSubjectMarks> Get()
        {
            return db.StudentsSubjectsMarksRepository.Get();
        }

        public IEnumerable<StudentSubjectMarks> GetByStudent(Student student)
        {
            return db.StudentsSubjectsMarksRepository.Get().Where(x => x.Student == student);
        }

        public StudentSubjectMarks GetById(int id)
        {
            return db.StudentsSubjectsMarksRepository.GetByID(id);
        }

        public StudentSubjectMarks GetByStudentAndSubject(Student student, Subject subject)
        {
            return Get().FirstOrDefault(x => x.Student == student && x.Subject == subject);
        }

        public StudentSubjectMarks Insert(Student student, Subject subject)
        {
            StudentSubjectMarks studentSubject = new StudentSubjectMarks
            {
                Student = student,
                Subject = subject
            };

            db.StudentsSubjectsMarksRepository.Insert(studentSubject);
            db.Save();

            return studentSubject;
        }

        public StudentSubjectMarks Update(StudentSubjectMarks studentSubject, Student student, Subject subject)
        {
            if (studentSubject.Subject != subject)
            {
                studentSubject.Subject = subject;
            }
            if (studentSubject.Student != student)
            {
                studentSubject.Student = student;
            }

            db.StudentsSubjectsMarksRepository.Update(studentSubject);
            db.Save();

            return studentSubject;
        }

        public StudentSubjectMarks AddTeacherSubject(StudentSubjectMarks studentSubject, TeacherSubject teacherSubject)
        {
            studentSubject.TeacherSubject = teacherSubject;

            db.StudentsSubjectsMarksRepository.Update(studentSubject);
            db.Save();

            return studentSubject;
        }

        public StudentSubjectMarks RemoveTeacherSubject(StudentSubjectMarks studentSubject, TeacherSubject teacherSubject)
        {
            teacherSubject.StudentSubjectMarks.Remove(studentSubject);
            db.TeachersSubjectsRepository.Update(teacherSubject);
            db.Save();

            return studentSubject;
        }

        public void Delete(StudentSubjectMarks studentSubject)
        {
            db.StudentsSubjectsMarksRepository.Delete(studentSubject);
            db.Save();
        }
    }
}