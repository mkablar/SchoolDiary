﻿using System.Collections.Generic;
using SchoolDiary.Models;
using SchoolDiary.Models.DTOs.UpdateDTOs;
using SchoolDiary.Models.DTOs.UserDTOs;

namespace SchoolDiary.Services
{
    public interface IAdminService : IGenericService<Admin>
    {
        Admin GetById(string id);
        Admin Update(Admin admin, UpdateAdminDTO userModel);
        // PublicUserDTO GetReturnDTO(string role, Admin admin);
        ICollection<string> GetLogs();
    }
}