﻿using System.Collections.Generic;

namespace SchoolDiary.Services
{
    public interface IGenericService<TEntity> where TEntity : class
    {
        TEntity Delete(TEntity entity);
        IEnumerable<TEntity> Get();
        // TEntity GetById(string id);
    }
}