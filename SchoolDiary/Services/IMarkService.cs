﻿using SchoolDiary.Models;
using SchoolDiary.Models.DTOs;
using SchoolDiary.Models.DTOs.UpdateDTOs;

namespace SchoolDiary.Services
{
    public interface IMarkService
    {
        Mark Delete(Mark mark);
        Mark GetById(int id);
        Mark Update(Mark mark, ApplicationUser grader, UpdateMarkDTO markModel);
        Mark Insert(ApplicationUser grader, StudentSubjectMarks studentSubject, MarkDTO markModel);
    }
}