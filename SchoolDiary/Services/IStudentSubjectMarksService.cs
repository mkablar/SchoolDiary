﻿using System.Collections.Generic;
using SchoolDiary.Models;

namespace SchoolDiary.Services
{
    public interface IStudentSubjectMarksService
    {
        void Delete(StudentSubjectMarks studentSubject);
        IEnumerable<StudentSubjectMarks> Get();
        StudentSubjectMarks GetById(int id);
        StudentSubjectMarks GetByStudentAndSubject(Student student, Subject subject);
        StudentSubjectMarks Insert(Student student, Subject subject);
        StudentSubjectMarks Update(StudentSubjectMarks studentSubject, Student student, Subject subject);
        StudentSubjectMarks AddTeacherSubject(StudentSubjectMarks studentSubject, TeacherSubject teacherSubject);
        StudentSubjectMarks RemoveTeacherSubject(StudentSubjectMarks studentSubject, TeacherSubject teacherSubject);
        IEnumerable<StudentSubjectMarks> GetByStudent(Student student);
    }
}