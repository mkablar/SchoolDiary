﻿using SchoolDiary.Models;
using SchoolDiary.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Services
{
    public class TeacherSubjectService : ITeacherSubjectService
    {
        private IUnitOfWork db;
        public TeacherSubjectService(IUnitOfWork db)
        {
            this.db = db;
        }

        public IEnumerable<TeacherSubject> Get()
        {
            return db.TeachersSubjectsRepository.Get();
        }

        public TeacherSubject GetById(int id)
        {
            return db.TeachersSubjectsRepository.GetByID(id);
        }

        public TeacherSubject GetByTeacherAndSubject(Teacher teacher, Subject subject)
        {
            return Get().FirstOrDefault(x => x.Teacher == teacher && x.Subject == subject);
        }

        public TeacherSubject Insert(Teacher teacher, Subject subject)
        {
            TeacherSubject teacherSubject = new TeacherSubject
            {
                Teacher = teacher,
                Subject = subject
            };

            db.TeachersSubjectsRepository.Insert(teacherSubject);
            db.Save();

            return teacherSubject;
        }

        public TeacherSubject Update(TeacherSubject teacherSubject, Teacher teacher, Subject subject)
        {
            if (teacherSubject.Subject != subject)
            {
                teacherSubject.Subject = subject;
            }
            if (teacherSubject.Teacher != teacher)
            {
                teacherSubject.Teacher = teacher;
            }

            db.TeachersSubjectsRepository.Update(teacherSubject);
            db.Save();

            return teacherSubject;
        }

        public void Delete(TeacherSubject teacherSubject)
        {
            db.TeachersSubjectsRepository.Delete(teacherSubject);
            db.Save();
        }
    }
}