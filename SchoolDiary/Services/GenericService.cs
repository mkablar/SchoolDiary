﻿using SchoolDiary.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Services
{
    public abstract class GenericService<TEntity> : IGenericService<TEntity> where TEntity : class
    {
        protected IUnitOfWork db;

        public abstract IEnumerable<TEntity> Get();
        // public abstract TEntity GetById(string id);
        public abstract TEntity Delete(TEntity entity);
    }
}