﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolDiary.Models;
using SchoolDiary.Models.DTOs;
using SchoolDiary.Models.DTOs.UserDTOs;
using SchoolDiary.Models.DTOs.UpdateDTOs;

namespace SchoolDiary.Services
{
    public interface IUserService : IGenericService<ApplicationUser>
    {
        Task<IdentityResult> RegisterAdmin(AdminUserDTO userModel);
        Task<IdentityResult> RegisterTeacher(TeacherUserDTO userModel);
        Task<IdentityResult> RegisterStudent(StudentUserDTO userModel);
        Task<IdentityResult> RegisterParent(ParentUserDTO userModel);
        ApplicationUser CreateUser(UserDTO userModel);
        ApplicationUser CreateAdultUser(ParentUserDTO userModel);
        ApplicationUser Update(ApplicationUser user, UpdateUserDTO userModel);
        ApplicationUser UpdateAdultUser(ApplicationUser user, UpdateAdultDTO userModel);
        ApplicationUser GetById(string id);
        bool EmailInDb(string email);
        bool UserNameInDb(string userName);
        bool UserInDb(string userName, out string message, string email = "");
        bool IsInDb(string id = "", string userName = "", string email = "");
        string GetCurrentUserRole(string id);
    }
}
