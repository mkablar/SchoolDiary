﻿using SchoolDiary.Models;
using SchoolDiary.Models.DTOs;
using SchoolDiary.Models.DTOs.UpdateDTOs;
using SchoolDiary.Models.DTOs.UserDTOs;
using SchoolDiary.Models.DTOs.UserDTOs.StudentDTOs;
using SchoolDiary.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Services
{
    public class StudentService : GenericService<Student>, IStudentService
    {
        private IUserService userService;
        private IStudentSubjectMarksService ssmService;
        private IMarkService markService;
        private IEmailService emailService;
        private IParentService parentService;
        private ITeacherService teacherService;

        public StudentService(IUnitOfWork db, IUserService userService, IStudentSubjectMarksService ssmService, IMarkService markService, IEmailService emailService, IParentService parentService, ITeacherService teacherService)
        {
            this.db = db;
            this.userService = userService;
            this.ssmService = ssmService;
            this.markService = markService;
            this.emailService = emailService;
            this.parentService = parentService;
            this.teacherService = teacherService;
        }

        public override IEnumerable<Student> Get()
        {
            return db.StudentsRepository.Get();
        }

        public Student GetById(string id)
        {
            try
            {
                return (Student)userService.GetById(id);
            } catch (Exception)
            {
                return null;
            }
        }

        public bool CheckIfParent(string parentId, Student student)
        {
            Parent parent = parentService.GetById(parentId);
            foreach (Parent p in student.Parents)
            {
                if (p == parent)
                {
                    return true;
                }
            }

            return false;
        }

        public bool CheckIfTeacher(string teacherId, Student student)
        {
            Teacher teacher = teacherService.GetById(teacherId);
            foreach (Teacher t in student.StudentSubjectMarks.Select(x => x.TeacherSubject.Teacher))
            {
                if (t == teacher)
                {
                    return true;
                }
            }

            return false;
        }

        public IEnumerable<StudentSubjectMarks> GetMarks(Student student)
        {
            return student.StudentSubjectMarks;
        }

        // public PublicUserDTO GetReturnDTO(string role, Student student)
        // {
        //     switch (role)
        //     {
        //         case "admins":
        //             return new AdminStudentDTO(student);
        //         case "teachers":
        //             return new TeacherStudentDTO(student);
        //         case "students":
        //             goto case "parents";
        //         case "parents":
        //             return new PublicStudentDTO(student);
        //         default:
        //             return new PublicUserDTO(student);
        //     }
        // }

        public Student Update(Student student, UpdateStudentDTO userModel)
        {
            student = (Student)userService.Update(student, userModel);

            db.StudentsRepository.Update(student);
            db.Save();

            return student;
        }

        public Student AddParent(Student student, Parent parent)
        {
            student.Parents.Add(parent);

            db.StudentsRepository.Update(student);
            db.Save();

            return student;
        }

        public Student RemoveParent(Student student, Parent parent)
        {
            student.Parents.Remove(parent);

            db.StudentsRepository.Update(student);
            db.Save();

            return student;
        }

        public Student AddSubject(Student student, Subject subject)
        {
            return ssmService.Insert(student, subject).Student;
        }

        public Student RemoveSubject(StudentSubjectMarks studentSubject)
        {
            Student student = studentSubject.Student;
            ssmService.Delete(studentSubject);
            return student;
        }

        public Student AddTeacherSubject(StudentSubjectMarks studentSubject, TeacherSubject teacherSubject)
        {
            return ssmService.AddTeacherSubject(studentSubject, teacherSubject).Student;
        }

        public Student RemoveTeacherSubject(StudentSubjectMarks studentSubject, TeacherSubject teacherSubject)
        {
            return ssmService.RemoveTeacherSubject(studentSubject, teacherSubject).Student;
        }

        public Student GiveMark(ApplicationUser grader, StudentSubjectMarks studentSubject, MarkDTO markModel)
        {
            Mark mark = markService.Insert(grader, studentSubject, markModel);
            studentSubject.Marks.Add(mark);
            db.StudentsSubjectsMarksRepository.Update(studentSubject);
            db.Save();

            emailService.SendEmail(mark);

            return studentSubject.Student;
        }

        public override Student Delete(Student student)
        {
            return (Student)userService.Delete(student);
        }
    }
}