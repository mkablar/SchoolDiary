﻿using System.Collections.Generic;
using SchoolDiary.Models;

namespace SchoolDiary.Services
{
    public interface ITeacherSubjectService
    {
        void Delete(TeacherSubject teacherSubject);
        IEnumerable<TeacherSubject> Get();
        TeacherSubject GetById(int id);
        TeacherSubject GetByTeacherAndSubject(Teacher teacher, Subject subject);
        TeacherSubject Insert(Teacher teacher, Subject subject);
        TeacherSubject Update(TeacherSubject teacherSubject, Teacher teacher, Subject subject);
    }
}