﻿using SchoolDiary.Models;
using SchoolDiary.Models.DTOs.UpdateDTOs;
using SchoolDiary.Models.DTOs.UserDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Services
{
    public interface ITeacherService : IGenericService<Teacher>
    {
        Teacher GetById(string id);
        Teacher Update(Teacher teacher, UpdateTeacherDTO userModel);
        // PublicUserDTO GetReturnDTO(string id, Teacher teacher);
        Teacher AddSubject(Teacher teacher, Subject subject);
        Teacher RemoveSubject(TeacherSubject teacherSubject);
        Teacher AddStudentSubject(TeacherSubject teacherSubject, StudentSubjectMarks studentSubject);
        Teacher RemoveStudentSubject(TeacherSubject teacherSubject, StudentSubjectMarks studentSubject);
    }
}