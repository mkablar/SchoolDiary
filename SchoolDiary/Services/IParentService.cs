﻿using SchoolDiary.Models;
using SchoolDiary.Models.DTOs.UpdateDTOs;
using SchoolDiary.Models.DTOs.UserDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDiary.Services
{
    public interface IParentService : IGenericService<Parent>
    {
        Parent GetById(string id);
        Parent Update(Parent parent, UpdateParentDTO userModel);
        // PublicUserDTO GetReturnDTO(string role, Parent parent);
    }
}
