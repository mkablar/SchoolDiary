﻿using SchoolDiary.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace SchoolDiary.Services
{
    public class EmailService : IEmailService
    {
        public void SendEmail(Mark mark)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["smtpServer"]);
                Student student = mark.StudentSubjectMarks.Student;

                mail.From = new MailAddress(ConfigurationManager.AppSettings["from"]);
                foreach (var parent in student.Parents)
                {
                    mail.To.Add(parent.Email);
                }
                mail.Subject = $"Your kid {student.FirstName} {student.LastName} got a {mark.Value} from {mark.StudentSubjectMarks.Subject.Name}";

                mail.IsBodyHtml = true;
                string htmlBody;

                htmlBody = string.Format(@"<h5>Your kid {0} {1} got a {2} from {3}</h5>
                    <div>
                        <h5>Teacher {4} {5} gave a description for the mark:</h5>
                        <p>{6}</p>
                    </div>",
                    student.FirstName, student.LastName, mark.Value, mark.StudentSubjectMarks.Subject.Name, mark.Grader.FirstName, mark.Grader.LastName, mark.Description);

                mail.Body = htmlBody;

                SmtpServer.Port = int.Parse(ConfigurationManager.AppSettings["smtpPort"]);
                SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["from"], ConfigurationManager.AppSettings["password"]);
                SmtpServer.EnableSsl = bool.Parse(ConfigurationManager.AppSettings["smtpSsl"]);

                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}