﻿using SchoolDiary.Models;

namespace SchoolDiary.Services
{
    public interface IEmailService
    {
        void SendEmail(Mark mark);
    }
}