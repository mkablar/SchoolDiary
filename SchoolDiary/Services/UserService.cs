﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using SchoolDiary.Models;
using SchoolDiary.Models.DTOs;
using SchoolDiary.Repositories;
using SchoolDiary.Models.DTOs.UserDTOs;
using SchoolDiary.Models.DTOs.UpdateDTOs;

namespace SchoolDiary.Services
{
    public class UserService : GenericService<ApplicationUser>, IUserService
    {
        public UserService(IUnitOfWork unitOfWork)
        {
            db = unitOfWork;
        }

        public override IEnumerable<ApplicationUser> Get()
        {
            return db.UsersRepository.Get();
        }

        public ApplicationUser GetById(string id)
        {
            return db.UsersRepository.GetByID(id);
        }

        public bool UserNameInDb(string userName)
        {
            return IsInDb(userName: userName);
        }

        public bool EmailInDb(string email)
        {
            return IsInDb(email: email);
        }

        public bool UserInDb(string userName, out string message, string email = "")
        {
            // string message = "";
            // bool check = false;
            // Tuple<bool, string> emailCheck = EmailInDb(userModel.Email);
            // Tuple<bool, string> userNameCheck = UserNameInDb(userModel.UserName);
            // if (emailCheck.Item1)
            // {
            //     check = true;
            //     if (userNameCheck.Item1)
            //     {
            //         message = "UserName and Email already in use.";
            //     } else
            //     {
            //         message = emailCheck.Item2;
            //     }
            // } else if (userNameCheck.Item1)
            // {
            //     check = true;
            //     message = userNameCheck.Item2;
            // }
            message = "already in use.";
            bool check = false;

            if (!string.IsNullOrEmpty(email) && EmailInDb(email)) {
                check = true;
                if (UserNameInDb(userName))
                {
                    message = $"Username and Email {message}";
                } else
                {
                    message = $"Email {message}";
                }
            } else if (UserNameInDb(userName))
            {
                check = true;
                message = $"UserName {message}";
            }

            return check;
        }

        public bool IsInDb(string id = "", string userName = "", string email = "")
        {
            return Get().Any(x => x.Id == id || x.UserName == userName || x.Email == email);
        }

        public ApplicationUser Update(ApplicationUser user, UpdateUserDTO userModel)
        {
            user.UserName = userModel.UserName;
            user.FirstName = userModel.FirstName;
            user.LastName = userModel.LastName;
            user.Address = userModel.Address;

            return user;
        }

        public ApplicationUser UpdateAdultUser(ApplicationUser user, UpdateAdultDTO userModel)
        {
            user = Update(user, userModel);
            user.Email = userModel.Email;
            user.PhoneNumber = userModel.PhoneNumber;

            return user;
        }

        public string GetCurrentUserRole(string id)
        {
            return "";
        }

        public ApplicationUser CreateAdultUser(ParentUserDTO userModel)
        {
            ApplicationUser user = CreateUser(userModel);
            user.Email = userModel.Email;
            user.PhoneNumber = userModel.PhoneNumber;

            return user;
        }

        public ApplicationUser CreateEmptyUser(string userRole)
        {
            switch (userRole)
            {
                case "admins":
                    return new Admin { };
                case "parents":
                    return new Parent { };
                case "students":
                    return new Student { };
                case "teachers":
                    return new Teacher { };
                default:
                    return null;
            }
        }

        public ApplicationUser CreateUser(UserDTO userModel)
        {
            ApplicationUser user = CreateEmptyUser(userModel.UserRole);

            user.UserName = userModel.UserName;
            user.FirstName = userModel.FirstName;
            user.LastName = userModel.LastName;
            user.Address = userModel.Address;

            return user;
        }

        public async Task<IdentityResult> RegisterAdmin(AdminUserDTO userModel)
        {
            Admin user = (Admin)CreateAdultUser(userModel);

            return await db.AuthRepository.RegisterAdminUser(user, userModel.Password);
        }

        public async Task<IdentityResult> RegisterTeacher(TeacherUserDTO userModel)
        {
            Teacher user = (Teacher)CreateAdultUser(userModel);

            return await db.AuthRepository.RegisterTeacherUser(user, userModel.Password);
        }

        public async Task<IdentityResult> RegisterStudent(StudentUserDTO userModel)
        {
            Student user = (Student)CreateUser(userModel);

            return await db.AuthRepository.RegisterStudentUser(user, userModel.Password);
        }

        public async Task<IdentityResult> RegisterParent(ParentUserDTO userModel)
        {
            Parent user = (Parent)CreateAdultUser(userModel);

            return await db.AuthRepository.RegisterParentUser(user, userModel.Password);
        }

        public override ApplicationUser Delete(ApplicationUser user)
        {
            db.UsersRepository.Delete(user);
            db.Save();

            return user;
        }
    }
}