﻿using SchoolDiary.Models;
using SchoolDiary.Models.DTOs.UpdateDTOs;
using SchoolDiary.Models.DTOs.UserDTOs;
using SchoolDiary.Models.DTOs.UserDTOs.TeacherDTOs;
using SchoolDiary.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Services
{
    public class TeacherService : GenericService<Teacher>, ITeacherService
    {
        private IUserService userService;
        private ITeacherSubjectService tsService;
        private IStudentSubjectMarksService ssmService;

        public TeacherService(IUnitOfWork db, IUserService userService, ITeacherSubjectService tsService, IStudentSubjectMarksService ssmService)
        {
            this.db = db;
            this.userService = userService;
            this.tsService = tsService;
            this.ssmService = ssmService;
        }

        public override IEnumerable<Teacher> Get()
        {
            return db.TeachersRepository.Get();
        }

        public Teacher GetById(string id)
        {
            try
            {
                return (Teacher)userService.GetById(id);
            } catch (Exception)
            {
                return null;
            }
        }

        // public PublicUserDTO GetReturnDTO(string role, Teacher teacher)
        // {
        //     switch (role)
        //     {
        //         case "admins":
        //             return new AdminTeacherDTO(teacher);
        //         case "teachers":
        //             return new TeacherTeacherDTO(teacher);
        //         case "students":
        //             goto case "parents";
        //         case "parents":
        //             return new PublicTeacherDTO(teacher);
        //         default:
        //             return new PublicUserDTO(teacher);
        //     }
        // }

        public Teacher Update(Teacher teacher, UpdateTeacherDTO userModel)
        {
            teacher = (Teacher)userService.UpdateAdultUser(teacher, userModel);

            db.TeachersRepository.Update(teacher);
            db.Save();

            return teacher;
        }

        public Teacher AddSubject(Teacher teacher, Subject subject)
        {
            return tsService.Insert(teacher, subject).Teacher;
        }

        public Teacher RemoveSubject(TeacherSubject teacherSubject)
        {
            Teacher teacher = teacherSubject.Teacher;
            tsService.Delete(teacherSubject);
            return teacher;
        }

        public Teacher AddStudentSubject(TeacherSubject teacherSubject, StudentSubjectMarks studentSubject)
        {
            return ssmService.AddTeacherSubject(studentSubject, teacherSubject).TeacherSubject.Teacher;
        }

        public Teacher RemoveStudentSubject(TeacherSubject teacherSubject, StudentSubjectMarks studentSubject)
        {
            studentSubject = ssmService.RemoveTeacherSubject(studentSubject, teacherSubject);

            return teacherSubject.Teacher;
        }

        public override Teacher Delete(Teacher teacher)
        {
            return (Teacher)userService.Delete(teacher);
        }
    }
}