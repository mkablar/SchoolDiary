﻿using SchoolDiary.Models;
using SchoolDiary.Models.DTOs.UpdateDTOs;
using SchoolDiary.Models.DTOs.UserDTOs;
using SchoolDiary.Models.DTOs.UserDTOs.AdminDTOs;
using SchoolDiary.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SchoolDiary.Services
{
    public class AdminService : GenericService<Admin>, IAdminService
    {
        private IUserService userService;

        public AdminService(IUnitOfWork db, IUserService userService)
        {
            this.db = db;
            this.userService = userService;
        }

        public override IEnumerable<Admin> Get()
        {
            return db.AdminsRepository.Get();
        }

        public Admin GetById(string id)
        {
            try
            {
                return (Admin)userService.GetById(id);
            } catch(Exception)
            {
                return null;
            }
        }

        // public PublicUserDTO GetReturnDTO(string role, Admin admin)
        // {
        //     switch (role)
        //     {
        //         case "admins":
        //             return new AdminAdminDTO(admin);
        //         case "teachers":
        //             return new TeacherAdminDTO(admin);
        //         case "students":
        //             goto case "parents";
        //         case "parents":
        //             return new PublicAdminDTO(admin);
        //         default:
        //             return new PublicUserDTO(admin);
        //     }
        // }

        public Admin Update(Admin admin, UpdateAdminDTO userModel)
        {
            admin = (Admin)userService.UpdateAdultUser(admin, userModel);
            admin.Address = userModel.Address;

            db.AdminsRepository.Update(admin);
            db.Save();

            return admin;
        }

        public ICollection<string> GetLogs()
        {
            StreamReader sr;
            string fileLocation = $"C:/Users/Mile/Projects/SchoolDiary/BackEnd/SchoolDiary/logs/app-log.txt";
            List<string> logs = new List<string>();

            try
            {
                sr = new StreamReader(fileLocation);
                while (true)
                {
                    string line = sr.ReadLine();
                    logs.Add(line);
                    if (string.IsNullOrEmpty(line))
                    {
                        break;
                    }
                }
                logs.Reverse();
                return logs;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override Admin Delete(Admin admin)
        {
            return (Admin)userService.Delete(admin);
        }
    }
}