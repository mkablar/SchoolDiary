﻿using SchoolDiary.Models;
using SchoolDiary.Models.DTOs.UpdateDTOs;
using SchoolDiary.Models.DTOs.UserDTOs;
using SchoolDiary.Models.DTOs.UserDTOs.ParentDTOs;
using SchoolDiary.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Services
{
    public class ParentService : GenericService<Parent>, IParentService
    {
        private IUserService userService;

        public ParentService(IUnitOfWork db, IUserService userService)
        {
            this.db = db;
            this.userService = userService;
        }

        public override IEnumerable<Parent> Get()
        {
            return db.ParentsRepository.Get();
        }

        public Parent GetById(string id)
        {
            try
            {
                return (Parent)userService.GetById(id);
            } catch (Exception)
            {
                return null;
            }
        }

        // public PublicUserDTO GetReturnDTO(string role, Parent parent)
        // {
        //     switch (role)
        //     {
        //         case "admins":
        //             return new AdminParentDTO(parent);
        //         case "teachers":
        //             return new TeacherParentDTO(parent);
        //         case "students":
        //             goto case "parents";
        //         case "parents":
        //             return new PublicParentDTO(parent);
        //         default:
        //             return new PublicUserDTO(parent);
        //     }
        // }

        public Parent Update(Parent parent, UpdateParentDTO userModel)
        {
            parent = (Parent)userService.UpdateAdultUser(parent, userModel);

            db.ParentsRepository.Update(parent);
            db.Save();

            return parent;
        }

        public override Parent Delete(Parent parent)
        {
            return (Parent)userService.Delete(parent);
        }
    }
}