﻿using SchoolDiary.Models;
using SchoolDiary.Models.DTOs;
using SchoolDiary.Models.DTOs.UpdateDTOs;
using SchoolDiary.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Services
{
    public class MarkService : IMarkService
    {
        private IUnitOfWork db;

        public MarkService(IUnitOfWork db)
        {
            this.db = db;
        }

        public Mark GetById(int id)
        {
            return db.MarksRepository.GetByID(id);
        }

        public Mark Insert(ApplicationUser grader, StudentSubjectMarks studentSubject, MarkDTO markModel)
        {
            Mark mark = new Mark
            {
                Grader = grader,
                StudentSubjectMarks = studentSubject,
                Description = markModel.Description != "" ? markModel.Description : $"{studentSubject.Student.FirstName} {studentSubject.Student.LastName} got a {markModel.Value} for displayed knowledge",
                Value = markModel.Value,
                MarkDate = DateTime.UtcNow
            };

            db.MarksRepository.Insert(mark);
            db.Save();

            return mark;
        }

        public Mark Update(Mark mark, ApplicationUser grader, UpdateMarkDTO markModel)
        {
            mark.Value = markModel.Value;
            mark.Description = markModel.Description;
            mark.Grader = grader;
            mark.MarkDate = DateTime.UtcNow;

            db.MarksRepository.Update(mark);
            db.Save();

            return mark;
        }

        public Mark Delete(Mark mark)
        {
            db.MarksRepository.Delete(mark);
            db.Save();

            return mark;
        }
    }
}