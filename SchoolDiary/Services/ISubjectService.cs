﻿using System.Collections.Generic;
using SchoolDiary.Models;
using SchoolDiary.Models.DTOs;
using SchoolDiary.Models.DTOs.UpdateDTOs;

namespace SchoolDiary.Services
{
    public interface ISubjectService
    {
        Subject AddTeacher(Teacher teacher, Subject subject);
        Subject Delete(Subject subject);
        IEnumerable<Subject> Get();
        Subject GetById(int id);
        Subject Insert(SubjectDTO subjectModel);
        Subject RemoveTeacher(TeacherSubject teacherSubject);
        Subject Update(Subject subject, UpdateSubjectDTO subjectModel);
        Subject AddStudent(Student student, Subject subject);
        Subject RemoveStudent(StudentSubjectMarks studentSubject);
    }
}