﻿using SchoolDiary.Models;
using SchoolDiary.Models.DTOs;
using SchoolDiary.Models.DTOs.UpdateDTOs;
using SchoolDiary.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Services
{
    public class SubjectService : ISubjectService
    {
        IUnitOfWork db;
        private ITeacherSubjectService tsService;
        private IStudentSubjectMarksService ssmService;

        public SubjectService(IUnitOfWork db, ITeacherSubjectService tsService, IStudentSubjectMarksService ssmService)
        {
            this.db = db;
            this.tsService = tsService;
            this.ssmService = ssmService;
        }

        public IEnumerable<Subject> Get()
        {
            return db.SubjectsRepository.Get();
        }

        public Subject GetById(int id)
        {
            return db.SubjectsRepository.GetByID(id);
        }

        public Subject Insert(SubjectDTO subjectModel)
        {
            Subject subject = new Subject
            {
                Name = subjectModel.Name,
                Description = subjectModel.Description,
                WeeklySubjectFund = subjectModel.WeeklySubjectFund
            };

            db.SubjectsRepository.Insert(subject);
            db.Save();

            return subject;
        }

        public Subject Update(Subject subject, UpdateSubjectDTO subjectModel)
        {
            subject.Name = subjectModel.Name;
            subject.Description = subjectModel.Description;

            db.SubjectsRepository.Update(subject);
            db.Save();

            return subject;
        }

        public Subject AddTeacher(Teacher teacher, Subject subject)
        {
            return tsService.Insert(teacher, subject).Subject;
        }

        public Subject RemoveTeacher(TeacherSubject teacherSubject)
        {
            Subject subject = teacherSubject.Subject;
            tsService.Delete(teacherSubject);
            return subject;
        }

        public Subject AddStudent(Student student, Subject subject)
        {
            return ssmService.Insert(student, subject).Subject;
        }

        public Subject RemoveStudent(StudentSubjectMarks studentSubject)
        {
            Subject subject = studentSubject.Subject;
            ssmService.Delete(studentSubject);
            return subject;
        }

        public Subject Delete(Subject subject)
        {
            db.SubjectsRepository.Delete(subject);
            db.Save();

            return subject;
        }
    }
}