﻿using SchoolDiary.Models;
using SchoolDiary.Models.DTOs;
using SchoolDiary.Models.DTOs.UpdateDTOs;
using SchoolDiary.Models.DTOs.UserDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolDiary.Services
{
    public interface IStudentService : IGenericService<Student>
    {
        Student GetById(string id);
        Student Update(Student student, UpdateStudentDTO userModel);
        // PublicUserDTO GetReturnDTO(string id, Student student);
        Student AddParent(Student student, Parent parent);
        Student RemoveParent(Student student, Parent parent);
        Student AddSubject(Student student, Subject subject);
        Student RemoveSubject(StudentSubjectMarks studentSubject);
        Student AddTeacherSubject(StudentSubjectMarks studentSubject, TeacherSubject teacherSubject);
        Student RemoveTeacherSubject(StudentSubjectMarks studentSubject, TeacherSubject teacherSubject);
        Student GiveMark(ApplicationUser grader, StudentSubjectMarks studentSubject, MarkDTO markModel);
        IEnumerable<StudentSubjectMarks> GetMarks(Student student);
        bool CheckIfParent(string parentId, Student student);
        bool CheckIfTeacher(string teacherId, Student student);
    }
}