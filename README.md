# School Diary BackEnd

BackEnd for Government of Republic of Serbia sponsored retraining course.

## Built With

* ASP.NET
* ASP.NET Web API
* Entity Framework
* Dependency Injection (Unity)
* NLog
* OAuth2
* MSSQL

## Author

* **Milan Kablar** - [mkablar](https://gitlab.com/mkablar)

## Acknowledgments

Thank you for all the lessons and help:

* [Ivan Vasiljević](https://github.com/ivanvs)
* [Mladen Kanostrevac](https://github.com/mkanostrevac)